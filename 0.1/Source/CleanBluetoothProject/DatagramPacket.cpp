
#include "DatagramPacket.h"


//DatagramPacket::DatagramPacket(void)
//{
//}
//
//
//DatagramPacket::~DatagramPacket(void)
//{
//}


void CreateDataStamp(Datagram&  DG)
{
 time_t rawtime;
 tm * timeinfo;

  time (&rawtime);
  timeinfo = localtime (&rawtime);

  DG.Header.Stamp[0] = timeinfo->tm_hour;
  DG.Header.Stamp[1] = timeinfo->tm_min;
  DG.Header.Stamp[2] = timeinfo->tm_sec;
  DG.Header.Stamp[3] = timeinfo->tm_wday;
  DG.Header.Stamp[4] = timeinfo->tm_mon;
  DG.Header.Stamp[5] = timeinfo->tm_year;
};
/************************************************************************************************/
Error FormatHeader( 
	const  DataType DT,
	Datagram& DataG
	)
{
	//sockaddr HostAdress;

	//getsockname(0,HostAdress,sizeof(sockaddr));

	if(DT == DataType::Test)
	{
		unsigned char is = 125;

		size_t Ds = sizeof(Datagram);
		size_t Hs = sizeof(Head);
		size_t DSs = sizeof(DataG.Data.DataBuffer);

		//memset(&DataG,0,sizeof(Datagram));
		//memcpy(&DataG.Header.DataType,&DT,sizeof(unsigned char));
		//memcpy(&DataG.Header.Sender,&is,sizeof(long));
		//memcpy(&DataG.Header.Receiver,&is,sizeof(long));
		//memcpy(&DataG.Header.HSize,&Hs,sizeof(size_t));
		//memcpy(&DataG.Data.DataSize,&DSs,sizeof(size_t));
		//memcpy(&DataG.DSize,&DSs,sizeof(size_t));
		DataG.Header.DataType = DT;
		//printf("Data Type %x\n",DataG.Head.DataType);

		DataG.Header.Sender = 1;
		//printf("Data Sender %x\n",DataG.Head.Sender);
	   DataG.Header.Receiver = 4;
		//printf("Data Receiver %x\n",DataG.Head.Receiver);
		DataG.DSize = sizeof(Datagram);
		DataG.Header.HSize = sizeof(Head);
		DataG.Data.DataSize = sizeof(Datagram);
		//printf("Data Type %x\n",DataG.DSize);
		CreateDataStamp(DataG);
		unsigned char CRCS = CRCXE((unsigned char*)&DataG.Header,sizeof(DataG.Header));
		DataG.Header.CRC = CRCS; 
		//printf("TIME STAMP %i h : %i m : %i d : %i wd  :\n",DataG.Head.Stamp[0],DataG.Head.Stamp[1], DataG.Head.Stamp[2],  DataG.Head.Stamp[3]  );
	}
	else
	{




		return Error::InvalidHeader;
	}

};
/************************************************************************************************/
Error FormatHeader( 
	const  DataType DT,
	Datagram& DataG,
	const size_t Sender,
    const size_t Receiver
	)
{
	//sockaddr HostAdress;

	//getsockname(0,HostAdress,sizeof(sockaddr));

	
		unsigned char is = 125;

		size_t Ds = sizeof(Datagram);
		size_t Hs = sizeof(Head);
		size_t DSs = sizeof(DataG.Data.DataBuffer);

		//memset(&DataG,0,sizeof(Datagram));
		//memcpy(&DataG.Header.DataType,&DT,sizeof(unsigned char));
		//memcpy(&DataG.Header.Sender,&is,sizeof(long));
		//memcpy(&DataG.Header.Receiver,&is,sizeof(long));
		//memcpy(&DataG.Header.HSize,&Hs,sizeof(size_t));
		//memcpy(&DataG.Data.DataSize,&DSs,sizeof(size_t));
		//memcpy(&DataG.DSize,&DSs,sizeof(size_t));
		DataG.Header.DataType = DT;
		//printf("Data Type %x\n",DataG.Head.DataType);

		DataG.Header.Sender = Sender;
		//printf("Data Sender %x\n",DataG.Head.Sender);
	   DataG.Header.Receiver = Receiver;
		//printf("Data Receiver %x\n",DataG.Head.Receiver);
		DataG.DSize = sizeof(Datagram);
		DataG.Header.HSize = sizeof(Head);
		DataG.Data.DataSize = sizeof(Datagram);
		//printf("Data Type %x\n",DataG.DSize);
		CreateDataStamp(DataG);
		unsigned char CRCS = CRCXE((unsigned char*)&DataG.Header,sizeof(DataG.Header));
		DataG.Header.CRC = CRCS; 
		//printf("TIME STAMP %i h : %i m : %i d : %i wd  :\n",DataG.Head.Stamp[0],DataG.Head.Stamp[1], DataG.Head.Stamp[2],  DataG.Head.Stamp[3]  );
	

		return Error::Success;

		
	};



/************************************************************************************************/
Error FormatData( 
	Datagram& DataG,
	BYTE* Data,
	size_t NumberBytes
	)
{

	if(Data == NULL || NumberBytes == 0)
	{
		return Error::EmptyData;

	}
	else
	{

	if(NumberBytes <= 512)
	{
		memcpy(&DataG.Data.DataBuffer,Data,NumberBytes);
	   
		DataG.Data.CRC = CRCXE(Data,NumberBytes);

		//for(size_t i = 0;i<NumberBytes;i++)
		//{
		//	DataG.Data.DataBuffer[i] = Data[i];

		//}

		return Error::Success;
	//sockaddr HostAdress;

	//getsockname(0,HostAdress,sizeof(sockaddr));

		//printf("TIME STAMP %i h : %i m : %i d : %i wd  :\n",DataG.Head.Stamp[0],DataG.Head.Stamp[1], DataG.Head.Stamp[2],  DataG.Head.Stamp[3]  );
	}
	else
	{

		return Error::BigDataIn;
	}


	}










};
/************************************************************************************************/




/************************************************************************************************/





/************************************************************************************************/

/************************************************************************************************/

Error FormatDatagramPacket( 
   DataType DT,
   Datagram& DataG,
   BYTE* Data,
   size_t  NumberofBytes
	)
{

	Error Ern;

	memset(&DataG,0,sizeof(Datagram));

	parallel_invoke(	

		[&]{Ern = FormatHeader(DT,DataG);},

	[&]{Ern = FormatData(DataG,Data,NumberofBytes);}
	);
	


	

	return Ern;


};

/************************************************************************************************/
   Error FormatDatagramPacket( 
   DataType DT,
   Datagram& DataG,
   size_t Sender,
   size_t Receiver,
   BYTE* Data,
   size_t  NumberofBytes
	)
{

	Error Ern;

	memset(&DataG,0,sizeof(Datagram));
	parallel_invoke(
	[&]{Ern = FormatHeader(DT,DataG,Sender,Receiver);},

	[&]{Ern = FormatData(DataG,Data,NumberofBytes);}
	);
	


	

	return Ern;


};


/************************************************************************************************/
unsigned char CRCXE(unsigned char* Data,size_t Size)
{
	//crcInit();
	/*unsigned char CRC = 0xBE;
	for(size_t i = 0;i>Size ;i++)
	{
	CRC&=Data[i];
	};

return	CRC^=0x3ff;*/
	//return crcFast(Data,Size);
	  int Summ = 0;

	  /////parallel_for(
	  int size = Size;
	  
	  parallel_for(0,size,1,
	  [&Summ,&Data](int i){
	  //////for (int i = 0; i < Size; i++){
      Summ += Data[i];
   });
   int SummH = (Summ >> 7) & 0x7F;

   BYTE CRC = (BYTE) (SummH + (Summ & 0x7F));

   CRC &= 0x7F;


   return CRC;



};



 unsigned char CRC8( unsigned char* Data,size_t Size)
{





	unsigned char  Result = 0;


	for(size_t i = Size;i>0;i--)
	{

		Result|=  (unsigned char)Data[i]<<i;

	}

	return Result;




};

