#pragma once
#ifndef _BLUETOOTH_RFCOMM_
#define _BLUETOOTH_RFCOMM_
#include <InitGuid.h>
#include <WinSock2.h>
#include <ws2bth.h>
#include <bthsdpdef.h>
#include <BluetoothAPIs.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <tchar.h>
#include <stdlib.h>
using namespace std;

/*****************************************************************************************************************************************
                                              Bluetooth Com port HAL
                                              Molotaliev 7.2.2015
Example:
 (Classic application)
 BluetoothRFCOMM Port = BluetoothRFCOMM ();
 Port.SetSearchPattern("DeviceFilter);
 Port.StartDiscovery();
 Port.GetDevicelist();
 //Choose you device
 Port.Write()
 Port.Read()

 Port.Close();
 //////////
 (CLR)
 BluetoothRFCOMM* Port = BluetoothRFCOMM ();
 Port->SetSearchPattern("DeviceFilter);
 Port->StartDiscovery();
 Port->Write()
 Port->Read()
 //
 (CXX)
 BluetoothRFCOMM^ Port = BluetoothRFCOMM ();
 Port->SetSearchPattern("DeviceFilter);
 Port->StartDiscovery();
 Port->Write()
 Port->Read()



******************************************************************************************************************************************/

typedef enum  BLUETOOTH_RFCOMM_MODE
{
BLUETOOTH_RFCOMM_MODE_CLIENT = 0x11,
BLUETOOTH_RFCOMM_MODE_SERVER = 0x12
}BRM;


typedef enum BLUETOOTH_RFCOMM_ERROR
{
    BLUETOOTH_RFCOMM_ERROR_SUCCESFULL,
	BLUETOOTH_RFCOMM_ERROR_BAD_RESULT,
    BLUETOOTH_RFCOMM_ERROR_NOT_INITIALIZE,
	BLUETOOTH_RFCOMM_ERROR_SEND_SUCCESFULL,
 	BLUETOOTH_RFCOMM_ERROR_RECEIVE_SUCCESFULL,
	BLUETOOTH_RFCOMM_ERROR_SEND_DATA_FORBIDEN,
	BLUETOOTH_RFCOMM_ERROR_RECEIVE_DATA_FORBIDED,
    BLUETOOTH_RFCOMM_ERROR_HW_DONGLE_NOT_FOUND,
    BLUETOOTH_RFCOMM_ERROR_STACK_NOT_ENABLED,
    BLUETOOTH_RFCOMM_ERROR_DISCOVERY_NULL_DEVICE_FOUND,
}BRE;





class BluetoothRFCOMM
{
public:
   BluetoothRFCOMM(void);
   virtual ~BluetoothRFCOMM(void);
   /******************************************************
   Client Side F(x)
   ********************************************************/
   BLUETOOTH_RFCOMM_ERROR Write(char* Data,size_t Length);
   BLUETOOTH_RFCOMM_ERROR Read(char* Data,size_t Length);
   bool Write(char *BufferW, DWORD BytesWrite, DWORD *BytesReturned);//For MyCommandClass Compability
   bool Read(char *BufferR, DWORD BytesToRead, DWORD *BytesReturned); //For MyCommandClass Compability
   bool WRcommand_BT (BYTE *DataSend, int NS, BYTE *DataRec, int NR, DWORD *NumRec, bool FlgDebugS, bool FlgDebugR);

   BLUETOOTH_RFCOMM_ERROR Connect(uint32_t DeviceID);
   BLUETOOTH_RFCOMM_ERROR Connect(string DeviceName);
   BLUETOOTH_RFCOMM_ERROR Connect(char* DeviceName);
   BLUETOOTH_RFCOMM_ERROR Connect(wstring DeviceName);
   //BLUETOOTH_RFCOMM_ERROR Connect(uint32_t DeviceIndex);

   BLUETOOTH_RFCOMM_ERROR Mode(BLUETOOTH_RFCOMM_MODE Mode);
   BLUETOOTH_RFCOMM_ERROR StartDiscovery(void);
   BLUETOOTH_RFCOMM_ERROR StartDiscoverServices(SOCKADDR_BTH* DeviceAddres );
   BLUETOOTH_RFCOMM_ERROR GetDeviceList(vector<SOCKADDR_BTH*> &DeviceList);
   
  /******************************************************
   Server Side F(x) - Partialy based BluetoothPacket Project
  ********************************************************/
   BLUETOOTH_RFCOMM_ERROR InitilizeServer(void);
   BLUETOOTH_RFCOMM_ERROR InitilizeServer(GUID ServiceUUID);
   BLUETOOTH_RFCOMM_ERROR BindConnection(void);
   BLUETOOTH_RFCOMM_ERROR StartServiceExtension(void);
   BLUETOOTH_RFCOMM_ERROR WaitConnection(void);
   BLUETOOTH_RFCOMM_ERROR AcceptConnection(void);
   
  



   BLUETOOTH_RFCOMM_ERROR SetFiendlyName(char* BtName);
   BLUETOOTH_RFCOMM_ERROR SetSearchPattern(const char* Pattern);
   BLUETOOTH_RFCOMM_ERROR SetSearchPattern(vector<string> Pattern);
   BLUETOOTH_RFCOMM_ERROR SetDataLoggingState(bool DataLoggingState);
   BLUETOOTH_RFCOMM_ERROR SetMaxCountDevicetoDiscover(size_t DeviceCountMaxtoDiscover);
   BLUETOOTH_RFCOMM_ERROR SetMinCountDevicetoDiscover(size_t DeviceCountMintoDiscover);



   BLUETOOTH_RFCOMM_ERROR GetFiendlyName(char* BtName);
   BLUETOOTH_RFCOMM_ERROR GetSearchPattern( char* Pattern);
   BLUETOOTH_RFCOMM_ERROR GetSearchPattern(vector<string> Pattern);
   BLUETOOTH_RFCOMM_ERROR GetDataLoggingState(bool DataLoggingState);
   BLUETOOTH_RFCOMM_ERROR GetBufferingState(bool BufferingState);
   BLUETOOTH_RFCOMM_ERROR GetMaxCountDevicetoDiscover(size_t DeviceCountMaxtoDiscover);
   BLUETOOTH_RFCOMM_ERROR GetMinCountDevicetoDiscover(size_t DeviceCountMintoDiscover);
   BLUETOOTH_RFCOMM_ERROR GetDiscoveredDevicesNames(vector<wstring>& _DeviceNames);
   BLUETOOTH_RFCOMM_ERROR GetDiscoveredDevicesNames(vector<string>& _DeviceNames);
   BLUETOOTH_RFCOMM_ERROR GetConnetedDeviceName( wstring& ConectedDeviceName);
   uint16_t GetDiscoveredDeviceCount(void);
   bool WriteOnly;
private: 
   bool _DataLoggingState;
   bool _BufferingState;
   bool _SinglePatern;
   int _BytesWrited_Readed;
   size_t _DeviceCountMaxtoDiscover;
   size_t _DeviceCountMintoDiscover;
 
   string _BtName;
   BRE _LastError;
   vector<string> _Pattern;
   vector<SOCKADDR_BTH> _Discovered_Device_Vector;
   vector<wstring> _Discovered_Device_Vector_Names;
   SOCKADDR_BTH _ConectedDevice;
   SOCKADDR_BTH _ServerBTH;
   GUID  _ServiceUUID;
   SOCKET _Connection;
   SOCKET _ConnectionServer;
   HANDLE _Radio; 
   BLUETOOTH_RFCOMM_MODE _Mode;
   uint16_t _Discovered;
  
};

#endif