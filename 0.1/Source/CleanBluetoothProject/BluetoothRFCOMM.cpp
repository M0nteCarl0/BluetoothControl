﻿#include "BluetoothRFCOMM.h"
 
 
using namespace std;
/**********************************************************************************************************************/
BluetoothRFCOMM::BluetoothRFCOMM(void):_DataLoggingState(true),_BufferingState(true),_DeviceCountMaxtoDiscover(9),_DeviceCountMintoDiscover(0),_Mode(BLUETOOTH_RFCOMM_MODE_CLIENT),_BytesWrited_Readed(0),_Discovered(0)
{

   BLUETOOTH_FIND_RADIO_PARAMS _BT_find_radio;
   HBLUETOOTH_RADIO_FIND _BT;
  
  _BT = NULL;

   WSADATA WDat;
   WORD Wd = MAKEWORD(2,0);
   _Pattern = vector<string>(_DeviceCountMaxtoDiscover);
   _Discovered_Device_Vector_Names = vector<wstring>(_DeviceCountMaxtoDiscover);
   _Discovered_Device_Vector = vector<SOCKADDR_BTH>(_DeviceCountMaxtoDiscover);
  
   _ServiceUUID = SerialPortServiceClass_UUID;
   _BT_find_radio.dwSize = sizeof(BLUETOOTH_FIND_RADIO_PARAMS);
   _BT = BluetoothFindFirstRadio(&_BT_find_radio, &_Radio);
  
    
     
      if(_BT != NULL)
      {
        printf("BluetoothFindFirstRadio() is OK!\n");
        
       _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
        
    
      }
      else
      {
      printf("BluetoothFindFirstRadio() failed with error code %d\n", GetLastError());
     _LastError = BLUETOOTH_RFCOMM_ERROR_HW_DONGLE_NOT_FOUND;
   
 
      }
   if(WSAStartup(Wd,&WDat)  == NO_ERROR  && _LastError ==  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL)
	  {
		printf("Wsa is %s\n",WDat.szSystemStatus);
      _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	  }
	else
	  {
      _LastError =  BLUETOOTH_RFCOMM_ERROR_NOT_INITIALIZE;
	  }
   if(_LastError == BLUETOOTH_RFCOMM_ERROR_SUCCESFULL)
   {

      _Connection =  socket(AF_BTH,SOCK_STREAM,BTHPROTO_RFCOMM);

      if(_Connection != 0)
      {
          _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;

      }
      else
      {

         _LastError =  BLUETOOTH_RFCOMM_ERROR_NOT_INITIALIZE;
       
      }

   }

 
}
/**********************************************************************************************************************/
BluetoothRFCOMM::~BluetoothRFCOMM(void)
{
   shutdown(_Connection,1);
   closesocket(_Connection);
   WSACleanup();
   if(_Mode == BLUETOOTH_RFCOMM_MODE_SERVER)
   {
	  shutdown(_ConnectionServer,1);
      closesocket(_ConnectionServer);

   }

    _Radio = NULL;
   _SinglePatern = false;

  
}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::Write(char* Data,size_t Length)
{
     int _Ds = 0;
	 _Ds = send(_Connection,(char*)Data,Length,0);
    _BytesWrited_Readed = _Ds;
     if( _Ds == Length)
	 {
        _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	 }
	 else
	 {
        _LastError = BLUETOOTH_RFCOMM_ERROR_SEND_DATA_FORBIDEN;
		 return BLUETOOTH_RFCOMM_ERROR_SEND_DATA_FORBIDEN;
	 }

};
/**********************************************************************************************************************/
bool BluetoothRFCOMM:: Write(char *BufferW, DWORD BytesToWrite, DWORD *BytesReturned)//For MyCommandClass Compability
{

   BRE Err = Write(BufferW,(size_t)BytesToWrite);
   *BytesReturned = _BytesWrited_Readed;
   if(Err == BLUETOOTH_RFCOMM_ERROR_SUCCESFULL)
   {

      return true;
   }
   else
   {

      return false;
   }

}
/**********************************************************************************************************************/
  bool BluetoothRFCOMM:: Read(char *BufferR, DWORD BytesToRead, DWORD *BytesReturned) //For MyCommandClass Compability
{



	int _Ds = 0;
int _BytesWrited_Readed = 0;

	do{

    _Ds =recv(_Connection,&BufferR[_BytesWrited_Readed],1,0);
	//Sleep(100);
    _BytesWrited_Readed+= _Ds;
	}
	while(_BytesWrited_Readed != BytesToRead);

    if( _BytesWrited_Readed == BytesToRead)
    {
  _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
    }
   else
   {
   _LastError =  BLUETOOTH_RFCOMM_ERROR_RECEIVE_DATA_FORBIDED;
	return BLUETOOTH_RFCOMM_ERROR_RECEIVE_DATA_FORBIDED;
   }




#if 0 
   //BRE Err = Read(BufferR,(size_t)BytesToRead);
   *BytesReturned = _BytesWrited_Readed;
   if(Err == BLUETOOTH_RFCOMM_ERROR_SUCCESFULL)
   {

      return true;
   }
   else
   {

      return false;
   }
#endif

  }

/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::Read(char* Data,size_t Length )
{
    int _Ds = 0;
    _Ds =recv(_Connection,Data,Length,0);
    _BytesWrited_Readed = _Ds;
    if(_Ds == Length)
    {
  _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
    }
   else
   {
   _LastError =  BLUETOOTH_RFCOMM_ERROR_RECEIVE_DATA_FORBIDED;
	return BLUETOOTH_RFCOMM_ERROR_RECEIVE_DATA_FORBIDED;
   }

};
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: SetFiendlyName(char* BtName)
{

 
    _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
   return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::SetSearchPattern(const char* Pattern)
{
   _SinglePatern = true;

   _Pattern[0] = Pattern;
   _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
   return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: SetSearchPattern(vector<string> Pattern)
{
   _Pattern = Pattern;
   _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
   return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: SetDataLoggingState(bool DataLoggingState)
{
   _DataLoggingState = DataLoggingState;
   _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
   return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::StartDiscovery(void)
{
    _Discovered = 0;
   wstring _DeviceName;
   string _DN;
 bool _PaternIsTrigered = false;
 bool _Result = true;
 


WSAQUERYSET wsaq;
  HANDLE hLookup;
  union {
  CHAR buf[5000];
   double __unused; 
  };
  LPWSAQUERYSET pwsaResults = (LPWSAQUERYSET) buf;
  DWORD dwSize  = sizeof(buf);
  ZeroMemory(&wsaq, sizeof(wsaq));
  wsaq.dwSize = sizeof(wsaq);
  wsaq.dwNameSpace = NS_BTH;
  wsaq.lpcsaBuffer = NULL;

  if (ERROR_SUCCESS != WSALookupServiceBegin (&wsaq, LUP_CONTAINERS|LUP_FLUSHCACHE , &hLookup))
  {
 
  }
  ZeroMemory(pwsaResults, sizeof(WSAQUERYSET));
  pwsaResults->dwSize = sizeof(WSAQUERYSET);
  pwsaResults->dwNameSpace = NS_BTH;
  pwsaResults->lpBlob = NULL;
  pwsaResults->dwNumberOfCsAddrs  = 0;
  while (ERROR_SUCCESS == WSALookupServiceNext (hLookup, LUP_RETURN_NAME | LUP_RETURN_ADDR  , &dwSize, pwsaResults))
  {
     if(_Discovered ==  _DeviceCountMaxtoDiscover)
     {
        goto EXIT;
     }
     memcpy(&_Discovered_Device_Vector[_Discovered],(SOCKADDR_BTH*)(pwsaResults->lpcsaBuffer->RemoteAddr.lpSockaddr),sizeof(SOCKADDR_BTH));
	 SOCKADDR_BTH* sa =(SOCKADDR_BTH*) pwsaResults->lpcsaBuffer->RemoteAddr.lpSockaddr;
	 _Discovered_Device_Vector[_Discovered].port =    sa->port;
    _Discovered_Device_Vector_Names[_Discovered] = pwsaResults->lpszServiceInstanceName ;

    _Discovered++;
    
  }
  EXIT:
  WSALookupServiceEnd(hLookup);

  return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;

}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: StartDiscoverServices(SOCKADDR_BTH* DeviceAddres )
{



   size_t _Discovered = 0;
   WCHAR Tex[1024];
   DWORD Size = sizeof(Tex);
   WSAAddressToStringW((LPSOCKADDR)DeviceAddres,(DWORD)sizeof(SOCKADDR_BTH),NULL,Tex,&Size);


   WSAQUERYSET wsaq;
  HANDLE hLookup;
  union {
  CHAR buf[5000];
   double __unused; // ensure proper alignment
  };
  LPWSAQUERYSET pwsaResults = (LPWSAQUERYSET) buf;
  DWORD dwSize  = sizeof(buf);
  BOOL bHaveName;
  ZeroMemory(&wsaq, sizeof(wsaq));
  wsaq.dwSize = sizeof(wsaq);
  wsaq.dwNameSpace = NS_BTH;
  wsaq.lpcsaBuffer = NULL;
  wsaq.lpServiceClassId = (LPGUID)&L2CAP_PROTOCOL_UUID;
  wsaq.lpszContext = Tex;
  wsaq.dwNumberOfCsAddrs = 0;
  if (ERROR_SUCCESS != WSALookupServiceBegin (&wsaq, LUP_CONTAINERS|LUP_FLUSHCACHE, &hLookup))
  {
  
    //return FALSE;
  }
  ZeroMemory(pwsaResults, sizeof(WSAQUERYSET));
  pwsaResults->dwSize = sizeof(WSAQUERYSET);
  pwsaResults->dwNameSpace = NS_BTH;
  pwsaResults->lpBlob = NULL;
  pwsaResults->lpServiceClassId  = (LPGUID)&L2CAP_PROTOCOL_UUID;
  pwsaResults->dwNumberOfCsAddrs  = 0;
  pwsaResults->lpszContext = Tex;

  while (ERROR_SUCCESS == WSALookupServiceNext (hLookup, LUP_RETURN_NAME | LUP_RETURN_ADDR, &dwSize, pwsaResults))
  {
    
     SOCKADDR_BTH* sa =(SOCKADDR_BTH*) pwsaResults->lpcsaBuffer->LocalAddr.lpSockaddr;
     wprintf(L"Service Name =  %s\n" ,pwsaResults->lpszServiceInstanceName);;
     wprintf(L"Service Port =  %i\n" ,sa->port);;
     bHaveName = pwsaResults->lpszServiceInstanceName && *(pwsaResults->lpszServiceInstanceName);
   

  
    
  }
  WSALookupServiceEnd(hLookup);

  return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;






}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::  SetMaxCountDevicetoDiscover(size_t DeviceCountMaxtoDiscover)
{
 _DeviceCountMaxtoDiscover  = DeviceCountMaxtoDiscover;
 _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
 return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;

}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR  BluetoothRFCOMM:: SetMinCountDevicetoDiscover(size_t DeviceCountMintoDiscover)
{
    _DeviceCountMintoDiscover =  DeviceCountMintoDiscover;
    return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
/**********************************************************************************************************************/
 BLUETOOTH_RFCOMM_ERROR  BluetoothRFCOMM:: GetFiendlyName(char* BtName)
{
   memcpy(BtName,_BtName.c_str(),_BtName.length());
  _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
   return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::  GetSearchPattern( char* Pattern)
{
   memcpy(Pattern,_Pattern[0].c_str(),_Pattern[0].length());
   return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
 /**********************************************************************************************************************/
   BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: GetSearchPattern(vector<string> Pattern)
   {

    Pattern = _Pattern; 
    return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;

   }
 /**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: GetDataLoggingState(bool DataLoggingState)
{
      DataLoggingState =   _DataLoggingState;
      _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
      return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
 /**********************************************************************************************************************/
   BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: GetBufferingState(bool BufferingState)
{
   BufferingState = _BufferingState;
  _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
   return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
/**********************************************************************************************************************/
   BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::   GetMaxCountDevicetoDiscover(size_t DeviceCountMaxtoDiscover)
{
       DeviceCountMaxtoDiscover = _DeviceCountMaxtoDiscover;
       return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
/**********************************************************************************************************************/
   BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::  GetMinCountDevicetoDiscover(size_t DeviceCountMintoDiscover)
{
      DeviceCountMintoDiscover = _DeviceCountMintoDiscover;
return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}
 /**********************************************************************************************************************/
   BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: WaitConnection(void)
   {
      int LER = 	listen(_ConnectionServer,0);
	if(LER == 0)
	{
  
   int _Size =sizeof(_ServerBTH); 
   if(SOCKET_ERROR == getsockname (_ConnectionServer, (SOCKADDR*)&_ServerBTH,  &_Size)) {  }
    printf( "listening on RFCOMM port: %d\n" , _ServerBTH.port  ) ;
	return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	}
	else
	{
    return BLUETOOTH_RFCOMM_ERROR_BAD_RESULT;
	}
  
}
  
   
   /**********************************************************************************************************************/
   BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM:: AcceptConnection(void)
   {
  
      int SSIZE = sizeof(SOCKADDR_BTH);
      SOCKADDR_BTH _Client;
         _Connection = accept(_ConnectionServer,(sockaddr*)&_Client,&SSIZE);
   if(_Connection !=0 )
	{

       _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
		return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	}
	else
	{
       _LastError = BLUETOOTH_RFCOMM_ERROR_BAD_RESULT;
		return BLUETOOTH_RFCOMM_ERROR_BAD_RESULT;


	}



   }
   /**********************************************************************************************************************/
    BLUETOOTH_RFCOMM_ERROR  BluetoothRFCOMM:: BindConnection(void)
    {



       size_t SSize = sizeof(_ServerBTH);
       int LER = 	bind(_ConnectionServer,(sockaddr*)&_ServerBTH,SSize);
	if(LER == 0)
	{
   _LastError =  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	}
	else
	{
      _LastError = BLUETOOTH_RFCOMM_ERROR_BAD_RESULT;
      printf("Error %i\n!",WSAGetLastError());
		return BLUETOOTH_RFCOMM_ERROR_BAD_RESULT;

	}
 

    }
 /**********************************************************************************************************************/

  BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::  GetDiscoveredDevicesNames(vector<string>& DeviceNames)
  {
     DeviceNames = vector<string>(_DeviceCountMaxtoDiscover);
     for(size_t i = 0;i<_DeviceCountMaxtoDiscover;i++)
     {
        DeviceNames[i]  = string((const char*)_Discovered_Device_Vector_Names[i].c_str() ,((sizeof(WCHAR)/sizeof(char) *  _Discovered_Device_Vector_Names[i].size())));

     }

	return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;

  }
  /**********************************************************************************************************************/
  BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::  GetDiscoveredDevicesNames(vector<wstring>& DeviceNames)
  {
      
	   return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;

  }
/**********************************************************************************************************************/

BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::GetDeviceList(vector<SOCKADDR_BTH*> &DeviceList)
{
  
   return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
};
/**********************************************************************************************************************/
 BLUETOOTH_RFCOMM_ERROR  BluetoothRFCOMM::  Connect(uint32_t DeviceID)
 {
	
	
    _Discovered_Device_Vector[DeviceID].addressFamily = AF_BTH;//addressFamily = AF_BTH;

	
   _Discovered_Device_Vector[DeviceID].serviceClassId = _ServiceUUID;
    _Discovered_Device_Vector[DeviceID].port = 0;
    
   // StartDiscoverServices(&_Discovered_Device_Vector[DeviceID]);
    int Err = connect(_Connection,(LPSOCKADDR)&_Discovered_Device_Vector[DeviceID],sizeof(SOCKADDR_BTH));
	if(Err == 0)
	{
		return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
	}
	else
		printf("Error %i\n!",WSAGetLastError());
		return BLUETOOTH_RFCOMM_ERROR_BAD_RESULT;

 }
 /**********************************************************************************************************************/
   BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::  Connect(string DeviceName)
 {
    
    wstring _Tmp((const wchar_t*)DeviceName.c_str(),((sizeof(char)/sizeof(wchar_t) *  DeviceName.size())));
    BRE _Error  =    Connect(_Tmp);
    return _Error;

 }
/**********************************************************************************************************************/
 BLUETOOTH_RFCOMM_ERROR BluetoothRFCOMM::  Connect(wstring DeviceName)
 {
#if 0
    uint32_t i = 0;
    for( i  = 0;i<_Discovered_Device_Vector.size() - 1;i++)
    {
       if(_Discovered_Device_Vector_Names[i].compare(0,_Discovered_Device_Vector_Names[i].length()  ,DeviceName) == 0)
       {

     break;

       }

    }
#endif
    // BRE _Error  =    Connect(i);
     return BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;

 }
 /**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR  BluetoothRFCOMM:: Connect(char* DeviceName)
{
   string _Temp(DeviceName);
   BRE _Error  =    Connect(_Temp);
   return _Error;

}
/**********************************************************************************************************************/
 BLUETOOTH_RFCOMM_ERROR  BluetoothRFCOMM:: InitilizeServer(void)
 {    closesocket(_Connection);
	 _ConnectionServer =  socket(AF_BTH,SOCK_STREAM,BTHPROTO_RFCOMM);
    _Mode  = BLUETOOTH_RFCOMM_MODE_SERVER;
    _ServerBTH.addressFamily =  AF_BTH;
    _ServerBTH.btAddr = 0;
    _ServerBTH.port = BT_PORT_ANY;
    _ServerBTH.serviceClassId = SerialPortServiceClass_UUID ;
    BRE _Error  =  BindConnection();

  return _Error;

 }
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR  BluetoothRFCOMM::InitilizeServer(GUID ServiceUUID)
{
    closesocket(_Connection);
    _Mode  = BLUETOOTH_RFCOMM_MODE_SERVER;
    _ServerBTH.addressFamily =  AF_BTH;
    _ServerBTH.btAddr = 0; 
   _ServerBTH.serviceClassId =  ServiceUUID; 
    BRE _Error  =  BindConnection();
    return _Error;

};
/**********************************************************************************************************************/
BLUETOOTH_RFCOMM_ERROR  BluetoothRFCOMM::StartServiceExtension(void)
{


 
CSADDR_INFO sockInfo ;
sockInfo . iProtocol = BTHPROTO_RFCOMM ;
sockInfo . iSocketType = SOCK_STREAM ;
sockInfo . LocalAddr . lpSockaddr = (LPSOCKADDR)&_ServerBTH ;
sockInfo . LocalAddr . iSockaddrLength = sizeof(_ServerBTH) ;
sockInfo . RemoteAddr . lpSockaddr = (LPSOCKADDR) &_ServerBTH ;
sockInfo . RemoteAddr . iSockaddrLength = sizeof(&_ServerBTH ) ;

WSAQUERYSET svcInfo = { 0 } ;
svcInfo . dwSize = sizeof(svcInfo ) ;
svcInfo . dwNameSpace = NS_BTH ;
svcInfo . lpszServiceInstanceName = L"Win32 SPP Service" ;
svcInfo . lpszComment = L"RFCOMM SPP" ;
svcInfo . lpServiceClassId = (LPGUID) &_ServiceUUID ;
svcInfo . dwNumberOfCsAddrs = 1;
svcInfo . lpcsaBuffer = &sockInfo ;
if( SOCKET_ERROR ==
WSASetService( &svcInfo , RNRSERVICE_REGISTER, 0 ) ) {
   return BLUETOOTH_RFCOMM_ERROR_BAD_RESULT;
}
else
{

return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;
}



}
/**********************************************************************************************************************/
 BLUETOOTH_RFCOMM_ERROR   BluetoothRFCOMM::  GetConnetedDeviceName( wstring& ConectedDeviceName)
 {


    //ConectedDeviceName = wstring(  inet_ntoa(  .sin_addr));
   
    return  BLUETOOTH_RFCOMM_ERROR_SUCCESFULL;

 };
 /**********************************************************************************************************************/
  uint16_t  BluetoothRFCOMM::  GetDiscoveredDeviceCount(void)
  {
	  return _Discovered;
  }
 /**********************************************************************************************************************/
 bool BluetoothRFCOMM::   WRcommand_BT (BYTE *DataSend, int NS, BYTE *DataRec, int NR, DWORD *NumRec, bool FlgDebugS, bool FlgDebugR)
{
  bool Ret = Write ((char*) DataSend, NS);
   //Sleep(500);
  if(WriteOnly  == false)
  {
   Read ((char*) DataRec, NR, NumRec);
  }
#if 0
	 do{
  Read ((char*) DataRec, NR, NumRec);
	}while(NR!=*NumRec);
#endif
return  Ret;
 }
 /**********************************************************************************************************************/