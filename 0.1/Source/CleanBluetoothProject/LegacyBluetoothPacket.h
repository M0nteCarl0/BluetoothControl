#pragma once
#include"BluetoothRFCOMM.h"
#include<Windows.h>
#include<time.h>
//#include"crc\crc.h"
#include <ppl.h>
using namespace Concurrency;
#ifndef _BLUETOOTH_LEGACY_
#define _BLUETOOTH_LEGACY_

typedef   UNALIGNED struct Head
	{
	unsigned char DataType;
	long Sender;
	long Receiver;
    unsigned char CRC;
	unsigned char Stamp[6];
	size_t HSize;
	}Head;
typedef UNALIGNED struct PData
	{
		unsigned char DataBuffer[512];
		unsigned char CRC:1;
		size_t DataSize:4;
	}PData;
   struct UNALIGNED Datagram
{
	/*Head Head;
	PData PData;*/


	Head Header;
 
	PData Data;


	size_t DSize;

};
enum Error:unsigned char
{
	Success,
	EmptyHeader,
	EmptyData,
	InvalidHeader,
	InvalidData,
	InvalidSize,
	NullStamp,
	BigDataIn
};
  enum DataType:unsigned char
{

	Instant,  //������
	WorkFlow, //�������
	Test      //��������

};


enum EthError:BYTE
{

	Succesful,
	BadResult,
	NotInitialise,
	SendSuccesful,
	ReceiveSuccesful,
	SendDataForbiden,
	ReceiveDataForbiden

};







Error FormatHeader( 
   DataType DT,
	Datagram& DataG
	);

Error FormatHeader( 
	const  DataType DT,
	Datagram& DataG,
	const size_t Sender,
    const size_t Receiver
	);

Error FormatData( 
	Datagram& DataG,
	BYTE* Data,
	size_t NumberBytes
	);

Error FormatDatagramPacket( 
   DataType DT,
   Datagram& DataG,
   BYTE* Data,
   size_t  NumberofBytes
	);

Error FormatDatagramPacket( 
   DataType DT,
   Datagram& DataG,
   size_t Sender,
   size_t Receiver,
   BYTE* Data,
   size_t  NumberofBytes
	);

   unsigned char CRC8( unsigned char* Data,size_t Size);
   unsigned char CRCXE(unsigned char* Data,size_t Size);



   
typedef  int  (*func)(BYTE* InParams,size_t Count); 


typedef func FunctionArray[128];







 void CommandParcer(Datagram& Datain,FunctionArray&   Functions);

 int  PrintMessage(BYTE* InParams,size_t Count);
 int  Byper(BYTE* InParams,size_t Count);
 int  PrintMessageAdd(BYTE* InParams,size_t Count);
 int  BotIndent(BYTE* InParams,size_t Count);
 int  SendString(BYTE* InParams,size_t Count);
 int  BotSearch(BYTE* InParams,size_t Count);

EthError SendDatagramBT(BluetoothRFCOMM& Connector,Datagram& Data);
EthError ReceiveDatagramBT(BluetoothRFCOMM& Connector,Datagram& Data);

void  IntToByte(INT& Source,BYTE* Dest);
void  ByteToInt(BYTE* Source, INT* Dest);
#endif










